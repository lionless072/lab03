# lab03

## Введение
В рамках данной работы было реализовано создание модуля python и публикация модуля в тестом pypi с использованием Docker и gitlab ci/cd

Работа состоит из двух частей:
 - Сборка модуля python
 - Публикация собранного модуля в тестовом pypi 

### Сборка модуля python
1. Создан корень модуля - pylib
2. В корне модуля создана директория с исходным кодом src
3. В качестве наполнения создан файл example.py с функцией super_useful_function внутри pylib/src
4. В корне модуля создан файл конфигурации сборки pyproject.toml
5. В корне проекта создан файл requirements.txt, где указаны все необходимые зависимости для сборки модуля
6. Создан докерфайл для сборки модуля (см. комментарии в Dockerfile) 
![docker](docs/docker.png)

7. Создан первый этап (build) в ci/cd описанный в .gitlab-ci.yml. Чтобы передать артефакты выполнения этого этапа, использована специальная конструкция artifacts

На этапе build происходит запуск построения докер-образа. Результат сборки – папка с файлами собранной библиотеки, которая затем передаётся в следующим этапе благодаря механизму artifacts.  
![build](docs/build.png)

### Публикая собранного модуля в тестовом pypi 
1. Создан докерфайл Upload.dockerfile, в котором описан процесс публикации модуля в тестовый pipy
![docker](docs/upload_docker.png)

2. Создан второй этап (upload) в ci/cd описаный в .gitlab-ci.yml. На этом этапе запускается сборка докер образ, в результате чего библиотека публикуется в тестовом pipy
![upload](docs/upload.png)


# Задание со звёздочкой

## Установка HashiCorp
установка через brew недоступна на территроии РФ.
Поэтому необходимо скомпилировать vault из исходного кода
```sh
git clone https://github.com/hashicorp/vault.git
cd vault
make bootstrap
make dev
```

## Запскаем HashiCorp
```sh
./bin/vault server -dev
```
В результате запущен vault по адресу 127.0.0.1:8200
![run_hashicorp](docs/run_hashicorp.png)

## Авторизируемся по токен в Vault
```sh
vault login
```

## Добавляем секрет в vault
```sh
./bin/vault kv put secret/pypi PYPITOKEN=token
```

## Настриваем политики доступа на чтение:
Создаем файл с настройками политики

![policy](docs/policy.png)

```sh
./bin/vault write pipy-secret-reader pypi-policy.hcl
```

## Запускаем ngrok
Для того, чтобы к vault был доступ раннерам гитлаба, необходимо опубликовать локально развёрнутый HashiCorp в сеть.
Для этого можно использовать утилиту ngrok

```sh
brew install ngrok/ngrok/ngrok
ngrok http 8200
```
После этого к хранилищу есть доступ из сети по адресу  https://7e2d-178-71-164-127.ngrok-free.app

## Устонавливаем переменны для vault в gitlab
![gitlab_vars](docs/gitlab_vars.png)

## Изменены шаг ci/cd
Перед этапом upload дополнительно запускается этап secret_getter, который забирает ключи из хранилища и кладёт их в файл build.env.
Следующий этап (upload) использует эту переменную, чтобы использовать для авторизации pypi.
![cicd](docs/cicd.png)


## Результат
В резльтате мы получили проект, для которого при каждом пуше в репозиторий будет автоматически собран модуль python и отправлен в тестоый pypi.



P.S. Иcпользование таким образом docker-функционала несколько избыточно (особенно из-за Dokcer-in-Docker), но для демонстрации этого достаточно/