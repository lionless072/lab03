FROM python:3.11

# Файл с настройками pypirc копируем в "дмашную" директорию
COPY .pypirc /root/.pypirc

# Устанавливаем  twine - библиотеку для публикации Python модулей.
RUN python -m pip install twine==4.0.2

# Копируем файлы сборки
COPY out  /build

# Загружаем собраный модуль
RUN python -m twine upload --repository testpypi /build/*
