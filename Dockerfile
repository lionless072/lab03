FROM python:3.11 as stage1
# Указываем рабочие директорию
WORKDIR /pylib

# копируем файл с зависимотсями
COPY requirements.txt .

# устанавливаем зависимотси
RUN python -m pip install --upgrade -r requirements.txt

# копируем исходный код и конфигурационые файлы
COPY pylib .

# собираем проект (по умолчанию будет создана папка dist, нас это утроит)
RUN python3 -m build

# для того чтобы резултат можно было без труда достаоть из сборочного докер конетейнера, создадим дополнительный обрыз,
# содержащий только нудные нам артифакты посторения подуля
FROM scratch AS export-stage
COPY --from=stage1 /pylib/dist .


